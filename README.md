To run the application:

`yarn` to install all packages
`yarn start` to start both client and server
The application is now running at `http://localhost:3000`
