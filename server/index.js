import 'isomorphic-fetch';
import express from 'express';
import bodyParser from 'body-parser';
import FormData from 'form-data';

import { get, post } from './request';

const app = express();
const port = process.env.PORT || 5000;

const apiBaseUrl = 'https://i2o3iefu55.execute-api.us-west-1.amazonaws.com/dev';

app.use(bodyParser.json());

app.get('/get-design-ids', async (request, response, next) => {
  try {
    const r = await get(`${apiBaseUrl}/get-design-ids`);
    if (r.success) {
      response.status(200).json({ designIds: r.design_ids });
    } else {
      response.sendStatus(500);
    }
  } catch (error) {
    next(error);
  }
});

app.post('/get-design-data', async (request, response, next) => {
  try {
    const { designId } = request.body;
    const form = new FormData();
    form.append('design_id', designId);
    const r = await post(`${apiBaseUrl}/get-design-data`, form);
    if (r.success) {
      response.status(200).json({ design: r.design });
    } else {
      response.sendStatus(500);
    }
  } catch (error) {
    next(error);
  }
});

app.listen(port, () => console.info(`Listening on port ${port}`));
