import React from 'react';

const Wall = ({
  wall,
  color,
  normaliseXValue,
  normaliseYValue,
}) => (
  <line
    x1={normaliseXValue(wall[0][0])}
    y1={normaliseYValue(wall[0][1])}
    x2={normaliseXValue(wall[1][0])}
    y2={normaliseYValue(wall[1][1])}
    strokeWidth={1}
    stroke={color}
  />
);

export default Wall;
