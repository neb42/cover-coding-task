import React from 'react';

const Exterior = ({
  exterior,
  color,
  normaliseXValue,
  normaliseYValue,
}) => {
  const exteriorPoints = exterior.reduce((acc, cur) => acc + `${normaliseXValue(cur[0])} ${normaliseYValue(cur[1])}, `, '').slice(0, -2);
  return (
    <polygon points={exteriorPoints} fill={color} />
  );
};

export default Exterior;
