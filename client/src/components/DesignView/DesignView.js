import React from 'react';

import Door from '../Door';
import Exterior from '../Exterior';
import Glass from '../Glass';
import Light from '../Light';
import Wall from '../Wall';

import * as Styles from './DesignView.styles';

export default class DesignView extends React.Component {

  normaliseXValue = value => {
    const { design: { building_exterior: buildingExterior }} = this.props;
    const minValue = Math.min(...buildingExterior.map(be => be[0]));
    const maxValue = Math.max(...buildingExterior.map(be => be[0]));
    const normalisedValue = ((value - minValue) / (maxValue - minValue)) * 100;
    return normalisedValue;
  }

  normaliseYValue = value => {
    const { design: { building_exterior: buildingExterior }} = this.props;
    const minValue = Math.min(...buildingExterior.map(be => be[1]));
    const maxValue = Math.max(...buildingExterior.map(be => be[1]));
    const normalisedValue = ((value - minValue) / (maxValue - minValue)) * 100;
    return normalisedValue;
  }

  render() {
    const {
      design: {
        building_exterior: buildingExterior,
        glass,
        walls,
        doors,
        lights,
      },
      legend,
    } = this.props;
    return (
      <Styles.Wrapper>
        <Styles.Svg width="100%" height="100%" viewBox="0 0 100 100" preserveAspectRatio="none">
          <Exterior
            exterior={buildingExterior}
            color={legend.exterior.color}
            normaliseXValue={this.normaliseXValue}
            normaliseYValue={this.normaliseYValue}
          />
          {lights.map((l, i) => (
            <Light
              key={`light-${i}`}
              light={l}
              color={legend.light.color}
              normaliseXValue={this.normaliseXValue}
              normaliseYValue={this.normaliseYValue}
            />
          ))}
          {walls.map((w, i) => (
            <Wall
              key={`wall-${i}`}
              wall={w}
              color={legend.wall.color}
              normaliseXValue={this.normaliseXValue}
              normaliseYValue={this.normaliseYValue}
            />
          ))}
          {glass.map((g, i) => (
            <Glass
              key={`glass-${i}`}
              glass={g}
              color={legend.glass.color}
              normaliseXValue={this.normaliseXValue}
              normaliseYValue={this.normaliseYValue}
            />
          ))}
          {doors.map((d, i) => (
            <Door
              key={`door-${i}`}
              door={d}
              color={legend.door.color}
              normaliseXValue={this.normaliseXValue}
              normaliseYValue={this.normaliseYValue}
            />
          ))}
        </Styles.Svg>
      </Styles.Wrapper>
    );
  }
}
