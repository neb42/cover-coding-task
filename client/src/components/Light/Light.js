import React from 'react';

const Light = ({
  light,
  color,
  normaliseXValue,
  normaliseYValue,
}) => (
  <line
    x1={normaliseXValue(light[0][0])}
    y1={normaliseYValue(light[0][1])}
    x2={normaliseXValue(light[1][0])}
    y2={normaliseYValue(light[1][1])}
    strokeWidth={1}
    stroke={color}
  />
);

export default Light;
