import styled from 'styled-components';

export const Wrapper = styled.div`
  display: grid;
  grid-template-columns: auto auto auto;
  grid-row-gap: 10px;
  grid-column-gap: 20px;
`;

export const LegendItem = styled.div`
  display: flex;
  align-items: center;
`;

export const Label = styled.span`
  color: #b4b4b4;
  font-weight: 400;
  font-size: 12px;
  margin-right: 5px;
`;

export const Swatch = styled.div`
  background-color: ${({ color }) => color};
  border-radius: 50%;
  width: 15px;
  height: 15px;
`;
