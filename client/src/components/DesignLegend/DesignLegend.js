import React from 'react';

import * as Styles from './DesignLegend.styles';

const DesignLegend = ({
  legend,
}) => (
  <Styles.Wrapper>
    {Object.keys(legend).map(key => (
      <Styles.LegendItem key={`legend-item-${key}`}>
        <Styles.Label>{legend[key].label}</Styles.Label>
        <Styles.Swatch color={legend[key].color} />
      </Styles.LegendItem>
    ))}
  </Styles.Wrapper>
);

export default DesignLegend;
