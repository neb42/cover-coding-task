import React from 'react';

import * as Styles from './DesignDropdown.styles';

const DesignDropdown = ({
  designIds,
  openDesignId,
  onSelectDesign,
}) => (
  <Styles.Wrapper>
    <Styles.Label htmlFor="dropdown-wrapper">Select a design:</Styles.Label>
    <Styles.Select name="dropdown-wrapper" value={openDesignId} onChange={event => onSelectDesign(event.target.value)}>
      <Styles.Option value="">None</Styles.Option>
      {designIds.map(id => (
        <Styles.Option key={`dropdown-option-${id}`} value={id}>
          Design {id}
        </Styles.Option>
      ))}
    </Styles.Select>
  </Styles.Wrapper>
);

export default DesignDropdown;
