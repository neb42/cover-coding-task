import styled from 'styled-components';

export const Wrapper = styled.div`

`;

export const Label = styled.label`
  color: #b4b4b4;
  font-weight: 400;
  margin-right: 10px;
`;

export const Select = styled.select`
`;

export const Option = styled.option`
`;
