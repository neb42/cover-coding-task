import React from 'react';

const Glass = ({
  glass,
  color,
  normaliseXValue,
  normaliseYValue,
}) => (
  <line
    x1={normaliseXValue(glass[0][0])}
    y1={normaliseYValue(glass[0][1])}
    x2={normaliseXValue(glass[1][0])}
    y2={normaliseYValue(glass[1][1])}
    strokeWidth={1}
    stroke={color}
  />
);

export default Glass;
