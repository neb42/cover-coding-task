import React from 'react';

const Door = ({
  door,
  color,
  normaliseXValue,
  normaliseYValue,
}) => (
  <line
    x1={normaliseXValue(door[0][0])}
    y1={normaliseYValue(door[0][1])}
    x2={normaliseXValue(door[1][0])}
    y2={normaliseYValue(door[1][1])}
    strokeWidth={1}
    stroke={color}
  />
);

export default Door;
