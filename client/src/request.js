class ResponseError extends Error {
  constructor(response) {
    super(`${response.status} ${response.statusText}`);
    this.name = '[http-status-error]';
    this.statusCode = response.status;
    this.response = response;
  }
}

const parseJSON = async response => {
  try {
    const body = await response.json();
    return body;
  } catch (e) {
    return Promise.resolve();
  }
};

const checkStatus = response => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  throw new ResponseError(response);
};

export const get = async url => {
  const response = await fetch(url, {
    method: 'GET',
  });
  const successfulResponse = await checkStatus(response);
  const responseBody = await parseJSON(successfulResponse);
  return responseBody;
};

export const post = async (url, body) => {
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  });
  const successfulResponse = await checkStatus(response);
  const responseBody = await parseJSON(successfulResponse);
  return responseBody;
};
