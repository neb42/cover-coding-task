import React from 'react';

import { get, post } from '../../request';
import DesignDropdown from '../../components/DesignDropdown';
import DesignView from '../../components/DesignView';
import DesignLegend from '../../components/DesignLegend';

import * as Styles from './App.styles';

const designLegend = {
  exterior: {
    label: 'Building exterior',
    color: '#d1d1d1',
  },
  glass: {
    label: 'Glass',
    color: '#a2d7e5',
  },
  wall: {
    label: 'Wall',
    color: '#ba4343',
  },
  door: {
    label: 'Door',
    color: '#259157',
  },
  light: {
    label: 'Light',
    color: '#efe940',
  },
};

export default class App extends React.Component<Props> {
  props;
  state = {
    isFetchingIdList: true,
    isFetchingDesign: false,
    openDesignId: '',
    designIds: [],
    design: null,
  };

  componentDidMount() {
    this.fetchDesignIds();
  }

  fetchDesignIds = async () => {
    const { designIds } = await get('/get-design-ids');
    this.setState({
      isFetchingIdList: false,
      designIds,
    });
  }

  fetchDesign = async designId => {
    const { design } = await post('/get-design-data', { designId });
    this.setState({
      isFetchingDesign: false,
      design,
    });
  }

  handleSelectDesign = designId => {
    this.setState({
      isFetchingDesign: designId ? true : false,
      openDesignId: designId,
      design: null,
    });
    designId && this.fetchDesign(designId);
  }

  render() {
    const { designIds, openDesignId, design, isFetchingIdList, isFetchingDesign } = this.state;
    return (
      <React.Fragment>
        <Styles.GlobalStyle />
        <Styles.Wrapper>
          <Styles.Header>Cover Coding Task</Styles.Header>
          {isFetchingIdList ? (
            <Styles.Loading />
          ) : (
            <React.Fragment>
              <Styles.DropwdownWrapper>
                <DesignDropdown
                  designIds={designIds}
                  openDesignId={openDesignId}
                  onSelectDesign={this.handleSelectDesign}
                />
              </Styles.DropwdownWrapper>
              <Styles.LegendWrapper>
                <DesignLegend legend={designLegend} />
              </Styles.LegendWrapper>
              <Styles.ViewWrapper>
                {isFetchingDesign ? (
                  <Styles.Loading />
                ) : design ? (
                  <DesignView design={design} legend={designLegend} />
                ) : (
                  <Styles.EmptyState>
                    Please select a design from the dropdown
                  </Styles.EmptyState>
                )}
              </Styles.ViewWrapper>
            </React.Fragment>
          )}
        </Styles.Wrapper>
      </React.Fragment>
    );
  }
}
