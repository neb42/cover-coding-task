import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  body {
    font-family: Averta,sans-serif;
    margin: 0;
  }
`;

export const Wrapper = styled.div`
  width: calc(100vw - 20px);
  height: calc(100vh - 20px);
  display: grid;
  grid-template-rows: 10% 10% 10% auto;
  justify-content: center;
  padding: 10px;
`;

export const Header = styled.h1`
  color: #00b3bf;
  font-weight: 400;
  text-align: center;
`;

export const DropwdownWrapper = styled.div`
  justify-self: center;
  align-self: center;
`;

export const LegendWrapper = styled.div`
  justify-self: center;
`;

export const ViewWrapper = styled.div`
  justify-self: center;
`;

export const Loading = styled.div`

`;

export const EmptyState = styled.div`
  color: #b4b4b4;
  font-weight: 400;
  border: 1px dashed #b4b4b4;
  padding: 50px;
  border-radius: 10px;
`;
